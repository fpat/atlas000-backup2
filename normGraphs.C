
// This program is to plot normalized integral to 1 with signal and background plots on one canvas


   
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <iostream>
//#include <TH2.h>
//#include <TStyle.h>
//#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TFile.h>
#include <Rtypes.h>
//#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <TreeReader.h>
#include <math.h>

void normGraphs(){

	/*TFile f1("Event.root");
	h_jet_massS->Draw("same");

	//gpad->SetLogy();	
	double norm = 1;
	double scale = norm / (h_jet_massS->Integral());
	h_jet_massS->Scale(scale);

	f1.Close();

	TFile f2("outputFile.root");
	h_jet_massB->Draw("same");

	//gpad->SetLogy();
	scale = norm / (h_jet_massB->Integral());
	h_jet_massB->Scale(scale);

	f2.Close();*/	

	cout << "Starting to combine" << endl;
	TH1D* h_sig = new TH1D;//("Signal Efficiency ; Background Efficiency");
	TH1D* h_bg = new TH1D; //edit: signal count total ; background count total
	TH1D* h_sig2 = new TH1D;
	TH1D* h_bg2= new TH1D;

	TFile f1("sigcut.root");
	h_sig = h_largeR_Zprime_matched_mass;
	h_sig->SetDirectory(0);
	f1.Close();

	//h_sig->Reset();	

	TFile f2("sigcut.root");
	h_bg = h_largeR_masscut;
	h_bg->SetDirectory(0);
	f2.Close();
	
	//h_bg->Reset();

	TFile f3("Event.root");
//f1.cd("h_jet_massS");

	//h_sig = h_jet_D2S_Cut;
	h_sig2 = h_jet_massS;
	h_sig2->SetDirectory(0);

	f3.Close();

	TFile f4("Event_C2_test.root");
//f2.cd("h_jet_mass_pt4");

	//h_bg = h_jet_D2B_Cut;
	h_bg2 = h_jet_massS_Cut_C2;
	h_bg2->SetDirectory(0);
   
	f4.Close();

//	const int n = 100;
	h_sig->Scale(1. / h_sig->Integral());
	h_bg->Scale(1. / h_bg->Integral());
	
	h_sig2->Scale(1. / h_sig2->Integral());
	h_bg2->Scale(1. / h_bg2->Integral());
	//h_sig->SetLineColor(2);
	//h_sig->Draw("same");
	//h_bg->Draw("same");
//	double allS = h_sig->Integral(0, n + 1);
   	
	TFile *f_out = new TFile("Normtau_rho.root", "RECREATE");
   
	f_out->cd();
	h_sig->SetLineColor(2);
	h_bg->SetLineColor(4);
	h_sig2->SetLineColor(1);
	h_bg2->SetLineColor(3);
	//h_sig->Write();
	h_sig->SetTitle("Signal matched #tau_{21}^{ddt}; #tau_{21}^{ddt}; number of jets");	
	h_bg->SetTitle("background #tau_{21}^{ddt}; #tau_{21}^{ddt}; number of jets");	
	h_bg2->SetTitle("Background Jet Mass");
	h_sig2->SetTitle("Background Jet Mass after #tau_{21}^{ddt} cut");

	//h_sig2->Draw("same");
	h_bg->Draw("same");
	//h_bg->Fit("gaus");	
	h_sig->Draw("same");
	//h_bg->Draw("same");
	//h_sig2->Draw("same");
	//h_bg2->Draw("same");	
	c1->BuildLegend();
/*	auto legend = new TLegend(.1,.7,.4,.9);
	legend->SetHeader("Normalized sigVSbg C2");
	legend->AddEntry(h_sig, "sig = blue line", "l");
	legend->AddEntry(h_bg, "bg = red line","l");
	legend->Draw("same");
*/
	
	//h_bg->Draw("same");
	//c1->SetName("Signal Mass Cut Tau21_wta");
	//gStyle->SetTitle("Signal Mass Cut Tau21_wta");
	TPaveText *title = new TPaveText(0.0,.9,.3,1.0,"brNDC");
	title->AddText("Jet Mass before and after Cut Mu12 at 0.13");
//	title->Draw("same");
	c1->Write();	
	f_out->Write();
	f_out->Close();
	
	cout <<"Finished"<< endl;
//	double allB = h_bg->Integral(0, n + 1);

}
