void test(){

	TH1D* h_sig = new TH1D;//("Signal Efficiency ; Background Efficiency");
	TH1D* h_bkg = new TH1D; //edit: signal count total ; background count total
	TH1D* lumi = new TH1D("lumi","expected lumi;number of events; number of toy-experiments",100,0,500);	
	TH1D* lumi2 = new TH1D("lumi2","bg expected lumi;number of events; number of toy-experiments",100,0,500);	

	TFile f1("sig4cut.root");

	h_sig = h_largeR_masscut;

	h_sig->SetDirectory(0);

	f1.Close();
	TFile f2("bg4cut.root");

	h_bkg = h_largeR_masscut;
	h_bkg->SetDirectory(0);
   
	f2.Close();
	TH1D* h_signif1 = new TH1D("bkg","bkg significance;number of events; number of toy-experiments",100,8000,9000);
	TH1D* h_signif2 = new TH1D("bkgsig","bkg+sig significance;number of events; number of toy-experiments",100,8000,9000);
	h_signif1->Reset();
	h_bkg->SetName("h_largeR_bgmass");	
	Double_t factor =.0039407;

	//h_sig->Scale(factor / h_sig->Integral());
//	h_bg->Scale(factor / h_bg->Integral());
	TH1D* source = new TH1D("source","source hist;x;y",100,0,30);		
	source->FillRandom("gaus",100000);
TH1D* h_fin1 = new TH1D("final","final histo",100,0,500);
TH1D* h_fin2 = new TH1D("final2","final histo",100,0,500);
 /* 
  TH1F* h_sig2 = new TH1F("h_sig","h_sig",100,0,100);
  TH1F* h_bkg = new TH1F("h_bkg","h_bkg",100,0,100);
  TH1F* h_fin1 = new TH1F("h_fin1","h_fin1",100,0,100);
  TH1F* h_fin2 = new TH1F("h_fin2","h_fin2",100,0,100);
  TH1F* h_signif1 = new TH1F("h_signif1","h_signif1",100,7000,11000);
  TH1F* h_signif2 = new TH1F("h_signif2","h_signif2",100,7000,11000);



  TRandom3 rnd1, rnd2;
  for (int i=0; i<10000; i++) h_sig->Fill(rnd1.Gaus(50,2));
  for (int i=0; i<1000000; i++) h_bkg->Fill(rnd2.Exp(100));
*/
  
  for (int k=0;k<1000;k++){ 

  
    if (k%10 == 0) cout << "Test: " << k << endl;
    h_fin1->Reset();
    h_fin2->Reset();

  //10749,792603
    for (int j=0;j<2247;j++){
      h_fin1->Fill(h_sig->GetRandom());
    }

    
    for (int j=0;j<346505;j++){
      h_fin2->Fill(h_bkg->GetRandom());
      h_fin1->Fill(h_bkg->GetRandom());
    }
 
    double bkg = (h_fin2->Integral(30,40))/5;
    double sig =(h_fin1->Integral(30,40))/5;

cout << bkg <<", "<< sig<<endl;
    h_signif1->Fill(bkg);
    h_signif2->Fill(sig);
  }
   TFile rFile("toy3.root","recreate");
//h_significance->SetLineColor(1);
cout << "Writing TGraph into root file" << endl;
h_fin1->Write();
h_sig->Write();
//lumi->Write();
//lumi2->Write();
h_bkg->Write();

//source->Write();
h_signif1->Write();
h_signif2->Write();
h_fin2->Write();

rFile.Write();
rFile.Close();

}

