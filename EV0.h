//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri May 30 09:04:20 2008 by ROOT version 5.18/00
// from TTree EV0/EV0
// found on file: zmm.aan.root
//////////////////////////////////////////////////////////

#ifndef EV0_h
#define EV0_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>

class EV0 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        MET_Final_ex;
   Double_t        MET_Final_ey;
   Double_t        MET_Final_et;
   Double_t        MET_Final_sumet;
   Double_t        MET_Truth_ex;
   Double_t        MET_Truth_ey;
   Double_t        MET_Truth_et;
   Double_t        MET_Truth_sumet;
   Double_t        MET_Truth_Muon_ex;
   Double_t        MET_Truth_Muon_ey;
   Double_t        MET_Truth_Muon_et;
   Double_t        MissingEx;
   Double_t        MissingEy;
   Double_t        MissingEt;
   Double_t        SumEt;
   Double_t        MissingExTruth;
   Double_t        MissingEyTruth;
   Double_t        MissingEtTruth;
   Double_t        SumEtTruth;
   Double_t        MissingExTruthMuon;
   Double_t        MissingEyTruthMuon;
   Double_t        MissingEtTruthMuon;
   Double_t        eventWeight;
   Double_t        eventWeightMCatNLO;
   Int_t           runNumber;
   Int_t           eventNumber;
   Int_t           All_N;
   Int_t           El_N;
   Int_t           Mu_N;
   Int_t           Ph_N;
   Int_t           PJet_N;
   Int_t           MissEt_N;
   std::vector<double>  *All_E;
   std::vector<double>  *All_p_T;
   std::vector<double>  *All_phi;
   std::vector<double>  *All_eta;
   std::vector<double>  *All_px;
   std::vector<double>  *All_py;
   std::vector<double>  *All_pz;
   std::vector<double>  *All_m;
   std::vector<double>  *All_charge;
   std::vector<double>  *All_pdgId;
   std::vector<double>  *El_E;
   std::vector<double>  *El_p_T;
   std::vector<double>  *El_phi;
   std::vector<double>  *El_eta;
   std::vector<double>  *El_px;
   std::vector<double>  *El_py;
   std::vector<double>  *El_pz;
   std::vector<double>  *El_m;
   std::vector<double>  *El_charge;
   std::vector<double>  *El_pdgId;
   std::vector<double>  *Mu_E;
   std::vector<double>  *Mu_p_T;
   std::vector<double>  *Mu_phi;
   std::vector<double>  *Mu_eta;
   std::vector<double>  *Mu_px;
   std::vector<double>  *Mu_py;
   std::vector<double>  *Mu_pz;
   std::vector<double>  *Mu_m;
   std::vector<double>  *Mu_charge;
   std::vector<double>  *Mu_pdgId;
   std::vector<double>  *Ph_E;
   std::vector<double>  *Ph_p_T;
   std::vector<double>  *Ph_phi;
   std::vector<double>  *Ph_eta;
   std::vector<double>  *Ph_px;
   std::vector<double>  *Ph_py;
   std::vector<double>  *Ph_pz;
   std::vector<double>  *Ph_m;
   std::vector<double>  *Ph_charge;
   std::vector<double>  *Ph_pdgId;
   std::vector<double>  *PJet_E;
   std::vector<double>  *PJet_p_T;
   std::vector<double>  *PJet_phi;
   std::vector<double>  *PJet_eta;
   std::vector<double>  *PJet_px;
   std::vector<double>  *PJet_py;
   std::vector<double>  *PJet_pz;
   std::vector<double>  *PJet_m;
   std::vector<double>  *PJet_charge;
   std::vector<double>  *PJet_pdgId;
   std::vector<double>  *MissEt_E;
   std::vector<double>  *MissEt_p_T;
   std::vector<double>  *MissEt_phi;
   std::vector<double>  *MissEt_eta;
   std::vector<double>  *MissEt_px;
   std::vector<double>  *MissEt_py;
   std::vector<double>  *MissEt_pz;
   std::vector<double>  *MissEt_m;
   std::vector<double>  *MissEt_charge;
   std::vector<double>  *MissEt_pdgId;
   Int_t           EVInstance;
   Int_t           EVCounter;
   Int_t           EVEventAndInstance;
   Int_t           EVRunNumber;
   Int_t           EVNInstance;

   // List of branches
   TBranch        *b_MET_Final_ex;   //!
   TBranch        *b_MET_Final_ey;   //!
   TBranch        *b_MET_Final_et;   //!
   TBranch        *b_MET_Final_sumet;   //!
   TBranch        *b_MET_Truth_ex;   //!
   TBranch        *b_MET_Truth_ey;   //!
   TBranch        *b_MET_Truth_et;   //!
   TBranch        *b_MET_Truth_sumet;   //!
   TBranch        *b_MET_Truth_Muon_ex;   //!
   TBranch        *b_MET_Truth_Muon_ey;   //!
   TBranch        *b_MET_Truth_Muon_et;   //!
   TBranch        *b_MissingEx;   //!
   TBranch        *b_MissingEy;   //!
   TBranch        *b_MissingEt;   //!
   TBranch        *b_SumEt;   //!
   TBranch        *b_MissingExTruth;   //!
   TBranch        *b_MissingEyTruth;   //!
   TBranch        *b_MissingEtTruth;   //!
   TBranch        *b_SumEtTruth;   //!
   TBranch        *b_MissingExTruthMuon;   //!
   TBranch        *b_MissingEyTruthMuon;   //!
   TBranch        *b_MissingEtTruthMuon;   //!
   TBranch        *b_eventWeight;   //!
   TBranch        *b_eventWeightMCatNLO;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_All_N;   //!
   TBranch        *b_El_N;   //!
   TBranch        *b_Mu_N;   //!
   TBranch        *b_Ph_N;   //!
   TBranch        *b_PJet_N;   //!
   TBranch        *b_MissEt_N;   //!
   TBranch        *b_All_E;   //!
   TBranch        *b_All_p_T;   //!
   TBranch        *b_All_phi;   //!
   TBranch        *b_All_eta;   //!
   TBranch        *b_All_px;   //!
   TBranch        *b_All_py;   //!
   TBranch        *b_All_pz;   //!
   TBranch        *b_All_m;   //!
   TBranch        *b_All_charge;   //!
   TBranch        *b_All_pdgId;   //!
   TBranch        *b_El_E;   //!
   TBranch        *b_El_p_T;   //!
   TBranch        *b_El_phi;   //!
   TBranch        *b_El_eta;   //!
   TBranch        *b_El_px;   //!
   TBranch        *b_El_py;   //!
   TBranch        *b_El_pz;   //!
   TBranch        *b_El_m;   //!
   TBranch        *b_El_charge;   //!
   TBranch        *b_El_pdgId;   //!
   TBranch        *b_Mu_E;   //!
   TBranch        *b_Mu_p_T;   //!
   TBranch        *b_Mu_phi;   //!
   TBranch        *b_Mu_eta;   //!
   TBranch        *b_Mu_px;   //!
   TBranch        *b_Mu_py;   //!
   TBranch        *b_Mu_pz;   //!
   TBranch        *b_Mu_m;   //!
   TBranch        *b_Mu_charge;   //!
   TBranch        *b_Mu_pdgId;   //!
   TBranch        *b_Ph_E;   //!
   TBranch        *b_Ph_p_T;   //!
   TBranch        *b_Ph_phi;   //!
   TBranch        *b_Ph_eta;   //!
   TBranch        *b_Ph_px;   //!
   TBranch        *b_Ph_py;   //!
   TBranch        *b_Ph_pz;   //!
   TBranch        *b_Ph_m;   //!
   TBranch        *b_Ph_charge;   //!
   TBranch        *b_Ph_pdgId;   //!
   TBranch        *b_PJet_E;   //!
   TBranch        *b_PJet_p_T;   //!
   TBranch        *b_PJet_phi;   //!
   TBranch        *b_PJet_eta;   //!
   TBranch        *b_PJet_px;   //!
   TBranch        *b_PJet_py;   //!
   TBranch        *b_PJet_pz;   //!
   TBranch        *b_PJet_m;   //!
   TBranch        *b_PJet_charge;   //!
   TBranch        *b_PJet_pdgId;   //!
   TBranch        *b_MissEt_E;   //!
   TBranch        *b_MissEt_p_T;   //!
   TBranch        *b_MissEt_phi;   //!
   TBranch        *b_MissEt_eta;   //!
   TBranch        *b_MissEt_px;   //!
   TBranch        *b_MissEt_py;   //!
   TBranch        *b_MissEt_pz;   //!
   TBranch        *b_MissEt_m;   //!
   TBranch        *b_MissEt_charge;   //!
   TBranch        *b_MissEt_pdgId;   //!
   TBranch        *b_EVInstance;   //!
   TBranch        *b_EVCounter;   //!
   TBranch        *b_EVEventAndInstance;   //!
   TBranch        *b_EVRunNumber;   //!
   TBranch        *b_EVNInstance;   //!

   EV0(TTree *tree=0);
   virtual ~EV0();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef EV0_cxx
EV0::EV0(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("zmm.aan.root");
      if (!f) {
         f = new TFile("zmm.aan.root");
      }
      tree = (TTree*)gDirectory->Get("EV0");

   }
   Init(tree);
}

EV0::~EV0()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t EV0::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t EV0::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (!fChain->InheritsFrom(TChain::Class()))  return centry;
   TChain *chain = (TChain*)fChain;
   if (chain->GetTreeNumber() != fCurrent) {
      fCurrent = chain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void EV0::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   All_E = 0;
   All_p_T = 0;
   All_phi = 0;
   All_eta = 0;
   All_px = 0;
   All_py = 0;
   All_pz = 0;
   All_m = 0;
   All_charge = 0;
   All_pdgId = 0;
   El_E = 0;
   El_p_T = 0;
   El_phi = 0;
   El_eta = 0;
   El_px = 0;
   El_py = 0;
   El_pz = 0;
   El_m = 0;
   El_charge = 0;
   El_pdgId = 0;
   Mu_E = 0;
   Mu_p_T = 0;
   Mu_phi = 0;
   Mu_eta = 0;
   Mu_px = 0;
   Mu_py = 0;
   Mu_pz = 0;
   Mu_m = 0;
   Mu_charge = 0;
   Mu_pdgId = 0;
   Ph_E = 0;
   Ph_p_T = 0;
   Ph_phi = 0;
   Ph_eta = 0;
   Ph_px = 0;
   Ph_py = 0;
   Ph_pz = 0;
   Ph_m = 0;
   Ph_charge = 0;
   Ph_pdgId = 0;
   PJet_E = 0;
   PJet_p_T = 0;
   PJet_phi = 0;
   PJet_eta = 0;
   PJet_px = 0;
   PJet_py = 0;
   PJet_pz = 0;
   PJet_m = 0;
   PJet_charge = 0;
   PJet_pdgId = 0;
   MissEt_E = 0;
   MissEt_p_T = 0;
   MissEt_phi = 0;
   MissEt_eta = 0;
   MissEt_px = 0;
   MissEt_py = 0;
   MissEt_pz = 0;
   MissEt_m = 0;
   MissEt_charge = 0;
   MissEt_pdgId = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("MET_Final_ex", &MET_Final_ex, &b_MET_Final_ex);
   fChain->SetBranchAddress("MET_Final_ey", &MET_Final_ey, &b_MET_Final_ey);
   fChain->SetBranchAddress("MET_Final_et", &MET_Final_et, &b_MET_Final_et);
   fChain->SetBranchAddress("MET_Final_sumet", &MET_Final_sumet, &b_MET_Final_sumet);
   fChain->SetBranchAddress("MET_Truth_ex", &MET_Truth_ex, &b_MET_Truth_ex);
   fChain->SetBranchAddress("MET_Truth_ey", &MET_Truth_ey, &b_MET_Truth_ey);
   fChain->SetBranchAddress("MET_Truth_et", &MET_Truth_et, &b_MET_Truth_et);
   fChain->SetBranchAddress("MET_Truth_sumet", &MET_Truth_sumet, &b_MET_Truth_sumet);
   fChain->SetBranchAddress("MET_Truth_Muon_ex", &MET_Truth_Muon_ex, &b_MET_Truth_Muon_ex);
   fChain->SetBranchAddress("MET_Truth_Muon_ey", &MET_Truth_Muon_ey, &b_MET_Truth_Muon_ey);
   fChain->SetBranchAddress("MET_Truth_Muon_et", &MET_Truth_Muon_et, &b_MET_Truth_Muon_et);
   fChain->SetBranchAddress("MissingEx", &MissingEx, &b_MissingEx);
   fChain->SetBranchAddress("MissingEy", &MissingEy, &b_MissingEy);
   fChain->SetBranchAddress("MissingEt", &MissingEt, &b_MissingEt);
   fChain->SetBranchAddress("SumEt", &SumEt, &b_SumEt);
   fChain->SetBranchAddress("MissingExTruth", &MissingExTruth, &b_MissingExTruth);
   fChain->SetBranchAddress("MissingEyTruth", &MissingEyTruth, &b_MissingEyTruth);
   fChain->SetBranchAddress("MissingEtTruth", &MissingEtTruth, &b_MissingEtTruth);
   fChain->SetBranchAddress("SumEtTruth", &SumEtTruth, &b_SumEtTruth);
   fChain->SetBranchAddress("MissingExTruthMuon", &MissingExTruthMuon, &b_MissingExTruthMuon);
   fChain->SetBranchAddress("MissingEyTruthMuon", &MissingEyTruthMuon, &b_MissingEyTruthMuon);
   fChain->SetBranchAddress("MissingEtTruthMuon", &MissingEtTruthMuon, &b_MissingEtTruthMuon);
   fChain->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
   fChain->SetBranchAddress("eventWeightMCatNLO", &eventWeightMCatNLO, &b_eventWeightMCatNLO);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("All_N", &All_N, &b_All_N);
   fChain->SetBranchAddress("El_N", &El_N, &b_El_N);
   fChain->SetBranchAddress("Mu_N", &Mu_N, &b_Mu_N);
   fChain->SetBranchAddress("Ph_N", &Ph_N, &b_Ph_N);
   fChain->SetBranchAddress("PJet_N", &PJet_N, &b_PJet_N);
   fChain->SetBranchAddress("MissEt_N", &MissEt_N, &b_MissEt_N);
   fChain->SetBranchAddress("All_E", &All_E, &b_All_E);
   fChain->SetBranchAddress("All_p_T", &All_p_T, &b_All_p_T);
   fChain->SetBranchAddress("All_phi", &All_phi, &b_All_phi);
   fChain->SetBranchAddress("All_eta", &All_eta, &b_All_eta);
   fChain->SetBranchAddress("All_px", &All_px, &b_All_px);
   fChain->SetBranchAddress("All_py", &All_py, &b_All_py);
   fChain->SetBranchAddress("All_pz", &All_pz, &b_All_pz);
   fChain->SetBranchAddress("All_m", &All_m, &b_All_m);
   fChain->SetBranchAddress("All_charge", &All_charge, &b_All_charge);
   fChain->SetBranchAddress("All_pdgId", &All_pdgId, &b_All_pdgId);
   fChain->SetBranchAddress("El_E", &El_E, &b_El_E);
   fChain->SetBranchAddress("El_p_T", &El_p_T, &b_El_p_T);
   fChain->SetBranchAddress("El_phi", &El_phi, &b_El_phi);
   fChain->SetBranchAddress("El_eta", &El_eta, &b_El_eta);
   fChain->SetBranchAddress("El_px", &El_px, &b_El_px);
   fChain->SetBranchAddress("El_py", &El_py, &b_El_py);
   fChain->SetBranchAddress("El_pz", &El_pz, &b_El_pz);
   fChain->SetBranchAddress("El_m", &El_m, &b_El_m);
   fChain->SetBranchAddress("El_charge", &El_charge, &b_El_charge);
   fChain->SetBranchAddress("El_pdgId", &El_pdgId, &b_El_pdgId);
   fChain->SetBranchAddress("Mu_E", &Mu_E, &b_Mu_E);
   fChain->SetBranchAddress("Mu_p_T", &Mu_p_T, &b_Mu_p_T);
   fChain->SetBranchAddress("Mu_phi", &Mu_phi, &b_Mu_phi);
   fChain->SetBranchAddress("Mu_eta", &Mu_eta, &b_Mu_eta);
   fChain->SetBranchAddress("Mu_px", &Mu_px, &b_Mu_px);
   fChain->SetBranchAddress("Mu_py", &Mu_py, &b_Mu_py);
   fChain->SetBranchAddress("Mu_pz", &Mu_pz, &b_Mu_pz);
   fChain->SetBranchAddress("Mu_m", &Mu_m, &b_Mu_m);
   fChain->SetBranchAddress("Mu_charge", &Mu_charge, &b_Mu_charge);
   fChain->SetBranchAddress("Mu_pdgId", &Mu_pdgId, &b_Mu_pdgId);
   fChain->SetBranchAddress("Ph_E", &Ph_E, &b_Ph_E);
   fChain->SetBranchAddress("Ph_p_T", &Ph_p_T, &b_Ph_p_T);
   fChain->SetBranchAddress("Ph_phi", &Ph_phi, &b_Ph_phi);
   fChain->SetBranchAddress("Ph_eta", &Ph_eta, &b_Ph_eta);
   fChain->SetBranchAddress("Ph_px", &Ph_px, &b_Ph_px);
   fChain->SetBranchAddress("Ph_py", &Ph_py, &b_Ph_py);
   fChain->SetBranchAddress("Ph_pz", &Ph_pz, &b_Ph_pz);
   fChain->SetBranchAddress("Ph_m", &Ph_m, &b_Ph_m);
   fChain->SetBranchAddress("Ph_charge", &Ph_charge, &b_Ph_charge);
   fChain->SetBranchAddress("Ph_pdgId", &Ph_pdgId, &b_Ph_pdgId);
   fChain->SetBranchAddress("PJet_E", &PJet_E, &b_PJet_E);
   fChain->SetBranchAddress("PJet_p_T", &PJet_p_T, &b_PJet_p_T);
   fChain->SetBranchAddress("PJet_phi", &PJet_phi, &b_PJet_phi);
   fChain->SetBranchAddress("PJet_eta", &PJet_eta, &b_PJet_eta);
   fChain->SetBranchAddress("PJet_px", &PJet_px, &b_PJet_px);
   fChain->SetBranchAddress("PJet_py", &PJet_py, &b_PJet_py);
   fChain->SetBranchAddress("PJet_pz", &PJet_pz, &b_PJet_pz);
   fChain->SetBranchAddress("PJet_m", &PJet_m, &b_PJet_m);
   fChain->SetBranchAddress("PJet_charge", &PJet_charge, &b_PJet_charge);
   fChain->SetBranchAddress("PJet_pdgId", &PJet_pdgId, &b_PJet_pdgId);
   fChain->SetBranchAddress("MissEt_E", &MissEt_E, &b_MissEt_E);
   fChain->SetBranchAddress("MissEt_p_T", &MissEt_p_T, &b_MissEt_p_T);
   fChain->SetBranchAddress("MissEt_phi", &MissEt_phi, &b_MissEt_phi);
   fChain->SetBranchAddress("MissEt_eta", &MissEt_eta, &b_MissEt_eta);
   fChain->SetBranchAddress("MissEt_px", &MissEt_px, &b_MissEt_px);
   fChain->SetBranchAddress("MissEt_py", &MissEt_py, &b_MissEt_py);
   fChain->SetBranchAddress("MissEt_pz", &MissEt_pz, &b_MissEt_pz);
   fChain->SetBranchAddress("MissEt_m", &MissEt_m, &b_MissEt_m);
   fChain->SetBranchAddress("MissEt_charge", &MissEt_charge, &b_MissEt_charge);
   fChain->SetBranchAddress("MissEt_pdgId", &MissEt_pdgId, &b_MissEt_pdgId);
   fChain->SetBranchAddress("EVInstance", &EVInstance, &b_EVInstance);
   fChain->SetBranchAddress("EVCounter", &EVCounter, &b_EVCounter);
   fChain->SetBranchAddress("EVEventAndInstance", &EVEventAndInstance, &b_EVEventAndInstance);
   fChain->SetBranchAddress("EVRunNumber", &EVRunNumber, &b_EVRunNumber);
   fChain->SetBranchAddress("EVNInstance", &EVNInstance, &b_EVNInstance);
   Notify();
}

Bool_t EV0::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void EV0::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t EV0::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef EV0_cxx
