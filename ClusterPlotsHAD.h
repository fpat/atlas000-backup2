// -*- c++ -*-

//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jun 30 12:03:53 2021 by ROOT version 6.14/04
// from TTree Had_tree/The Hadronic tree
// found on file: results.root
//////////////////////////////////////////////////////////

#ifndef ClusterPlotsHAD_h
#define ClusterPlotsHAD_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <string>
#include <tuple>

#include <cstdio>

// Header file for the classes stored in the TTree if any.

class ClusterPlotsHAD {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           runNumber;
   Int_t           eventNumber;
   Double_t        truthE;
   Double_t        truthPt;
   Double_t        truthEta;
   Double_t        truthPhi;
   Int_t           truthPDG;
   Int_t           nCluster;
   Int_t           clusterIndex;
   Int_t           cluster_nCells;
   Int_t           cluster_nCells_tot;
   Int_t           clusterIndex_1;
   Double_t        clusterECalib;
   Double_t        clusterPtCalib;
   Double_t        clusterEtaCalib;
   Double_t        clusterPhiCalib;
   Double_t        cluster_sumCellECalib;
   Double_t        clusterE;
   Double_t        clusterPt;
   Double_t        clusterEta;
   Double_t        clusterPhi;
   Double_t        cluster_sumCellE;
   Double_t        cluster_EM_PROBABILITY;
   Double_t        Blacluster_HAD_WEIGHTnk;
   Double_t        cluster_OOC_WEIGHT;
   Double_t        cluster_DM_WEIGHT;
   Double_t        cluster_ENG_CALIB_TOT;
   Double_t        cluster_ENG_CALIB_OUT_T;
   Double_t        cluster_ENG_CALIB_DEAD_TOT;
   Double_t        cluster_CENTER_MAG;
   Double_t        cluster_FIRST_ENG_DENS;
   Double_t        cluster_FIRST_PHI;
   Double_t        cluster_FIRST_ETA;
   Double_t        cluster_SECOND_R;
   Double_t        cluster_SECOND_LAMBDA;
   Double_t        cluster_DELTA_PHI;
   Double_t        cluster_DELTA_THETA;
   Double_t        cluster_DELTA_ALPHA;
   Double_t        cluster_CENTER_X;
   Double_t        cluster_CENTER_Y;
   Double_t        cluster_CENTER_Z;
   Double_t        cluster_CENTER_LAMBDA;
   Double_t        cluster_LATERAL;
   Double_t        cluster_LONGITUDINAL;
   Double_t        cluster_ENG_FRAC_EM;
   Double_t        cluster_ENG_FRAC_MAX;
   Double_t        cluster_ENG_FRAC_CORE;
   Double_t        cluster_SECOND_ENG_DENS;
   Double_t        cluster_ISOLATION;
   Double_t        cluster_ENG_BAD_CELLS;
   Double_t        cluster_N_BAD_CELLS;
   Double_t        cluster_N_BAD_CELLS_CORR;
   Double_t        cluster_BAD_CELLS_CORR_E;
   Double_t        cluster_BADLARQ_FRAC;
   Double_t        cluster_ENG_POS;
   Double_t        cluster_SIGNIFICANCE;
   Double_t        cluster_CELL_SIGNIFICANCE;
   Double_t        cluster_CELL_SIG_SAMPLING;
   Double_t        cluster_AVG_LAR_Q;
   Double_t        cluster_AVG_TILE_Q;
   Double_t        cluster_ENG_BAD_HV_CELLS;
   Double_t        cluster_N_BAD_HV_CELLS;
   Double_t        cluster_PTD;
   Double_t        cluster_MASS;
   Double_t        EM_Shower;
   Double_t        EM_Pro;
   Double_t        CalibratedE;
   Double_t        Delta_E;
   Double_t        Delta_Calib_E;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_truthE;   //!
   TBranch        *b_truthPt;   //!
   TBranch        *b_truthEta;   //!
   TBranch        *b_truthPhi;   //!
   TBranch        *b_truthPDG;   //!
   TBranch        *b_nCluster;   //!
   TBranch        *b_clusterIndex;   //!
   TBranch        *b_cluster_nCells;   //!
   TBranch        *b_cluster_nCells_tot;   //!
   TBranch        *b_clusterIndex_1;   //!
   TBranch        *b_clusterECalib;   //!
   TBranch        *b_clusterPtCalib;   //!
   TBranch        *b_clusterEtaCalib;   //!
   TBranch        *b_clusterPhiCalib;   //!
   TBranch        *b_cluster_sumCellECalib;   //!
   TBranch        *b_clusterE;   //!
   TBranch        *b_clusterPt;   //!
   TBranch        *b_clusterEta;   //!
   TBranch        *b_clusterPhi;   //!
   TBranch        *b_cluster_sumCellE;   //!
   TBranch        *b_cluster_EM_PROBABILITY;   //!
   TBranch        *b_cluster_HAD_WEIGHT;   //!
   TBranch        *b_cluster_OOC_WEIGHT;   //!
   TBranch        *b_cluster_DM_WEIGHT;   //!
   TBranch        *b_cluster_ENG_CALIB_TOT;   //!
   TBranch        *b_cluster_ENG_CALIB_OUT_T;   //!
   TBranch        *b_cluster_ENG_CALIB_DEAD_TOT;   //!
   TBranch        *b_cluster_CENTER_MAG;   //!
   TBranch        *b_cluster_FIRST_ENG_DENS;   //!
   TBranch        *b_cluster_FIRST_PHI;   //!
   TBranch        *b_cluster_FIRST_ETA;   //!
   TBranch        *b_cluster_SECOND_R;   //!
   TBranch        *b_cluster_SECOND_LAMBDA;   //!
   TBranch        *b_cluster_DELTA_PHI;   //!
   TBranch        *b_cluster_DELTA_THETA;   //!
   TBranch        *b_cluster_DELTA_ALPHA;   //!
   TBranch        *b_cluster_CENTER_X;   //!
   TBranch        *b_cluster_CENTER_Y;   //!
   TBranch        *b_cluster_CENTER_Z;   //!
   TBranch        *b_cluster_CENTER_LAMBDA;   //!
   TBranch        *b_cluster_LATERAL;   //!
   TBranch        *b_cluster_LONGITUDINAL;   //!
   TBranch        *b_cluster_ENG_FRAC_EM;   //!
   TBranch        *b_cluster_ENG_FRAC_MAX;   //!
   TBranch        *b_cluster_ENG_FRAC_CORE;   //!
   TBranch        *b_cluster_SECOND_ENG_DENS;   //!
   TBranch        *b_cluster_ISOLATION;   //!
   TBranch        *b_cluster_ENG_BAD_CELLS;   //!
   TBranch        *b_cluster_N_BAD_CELLS;   //!
   TBranch        *b_cluster_N_BAD_CELLS_CORR;   //!
   TBranch        *b_cluster_BAD_CELLS_CORR_E;   //!
   TBranch        *b_cluster_BADLARQ_FRAC;   //!
   TBranch        *b_cluster_ENG_POS;   //!
   TBranch        *b_cluster_SIGNIFICANCE;   //!
   TBranch        *b_cluster_CELL_SIGNIFICANCE;   //!
   TBranch        *b_cluster_CELL_SIG_SAMPLING;   //!
   TBranch        *b_cluster_AVG_LAR_Q;   //!
   TBranch        *b_cluster_AVG_TILE_Q;   //!
   TBranch        *b_cluster_ENG_BAD_HV_CELLS;   //!
   TBranch        *b_cluster_N_BAD_HV_CELLS;   //!
   TBranch        *b_cluster_PTD;   //!
   TBranch        *b_cluster_MASS;   //!
   TBranch        *b_EM_Shower;   //!
   TBranch        *b_EM_Pro;   //!
   TBranch        *b_CalibratedE;   //!
   TBranch        *b_Delta_E;   //!
   TBranch        *b_Delta_Calib_E;   //!

   ClusterPlotsHAD(TTree *tree=0);
   virtual ~ClusterPlotsHAD();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(Long64_t kentries=-1);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

private:

  std::string _inputFileName[1][1]  = {"newresults.root"};
  std::string _outputFileName[1][1] = { "ClusterPlotsHAD.root" };
  std::string _treeName[1][1]       = { "Had_tree"             };

  std::tuple<double,double> _rapidityRange = { -0.7, 0.7 };
  bool inRapRange(double y) { return y > std::get<0>(_rapidityRange) && y < std::get<1>(_rapidityRange); } 


};

#endif

#ifdef ClusterPlotsHAD_cxx
ClusterPlotsHAD::ClusterPlotsHAD(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
     TFile* f = new TFile(_inputFileName.c_str(),"READ");
     if ( f      == nullptr ) { printf("[ClusterPlotsHAD] cannot open input file \042%s\042\n",_inputFileName.c_str()); delete this; }
     f->GetObject(_treeName.c_str(),tree);
     if ( tree == nullptr ) { printf("[ClusterPlotsHAD] cannot find tree \042%s\042\n",_treeName.c_str()); delete this; }
     printf("[ClusterPlotsHAD] allocated tree \042%s\042 from file \042%s\042\n",tree->GetName(),f->GetName());
   }
   Init(tree);
}

ClusterPlotsHAD::~ClusterPlotsHAD()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ClusterPlotsHAD::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ClusterPlotsHAD::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ClusterPlotsHAD::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("truthE", &truthE, &b_truthE);
   fChain->SetBranchAddress("truthPt", &truthPt, &b_truthPt);
   fChain->SetBranchAddress("truthEta", &truthEta, &b_truthEta);
   fChain->SetBranchAddress("truthPhi", &truthPhi, &b_truthPhi);
   fChain->SetBranchAddress("truthPDG", &truthPDG, &b_truthPDG);
   fChain->SetBranchAddress("nCluster", &nCluster, &b_nCluster);
   fChain->SetBranchAddress("clusterIndex", &clusterIndex, &b_clusterIndex);
   fChain->SetBranchAddress("cluster_nCells", &cluster_nCells, &b_cluster_nCells);
   fChain->SetBranchAddress("cluster_nCells_tot", &cluster_nCells_tot, &b_cluster_nCells_tot);
   fChain->SetBranchAddress("clusterIndex_1", &clusterIndex_1, &b_clusterIndex_1);
   fChain->SetBranchAddress("clusterECalib", &clusterECalib, &b_clusterECalib);
   fChain->SetBranchAddress("clusterPtCalib", &clusterPtCalib, &b_clusterPtCalib);
   fChain->SetBranchAddress("clusterEtaCalib", &clusterEtaCalib, &b_clusterEtaCalib);
   fChain->SetBranchAddress("clusterPhiCalib", &clusterPhiCalib, &b_clusterPhiCalib);
   fChain->SetBranchAddress("cluster_sumCellECalib", &cluster_sumCellECalib, &b_cluster_sumCellECalib);
   fChain->SetBranchAddress("clusterE", &clusterE, &b_clusterE);
   fChain->SetBranchAddress("clusterPt", &clusterPt, &b_clusterPt);
   fChain->SetBranchAddress("clusterEta", &clusterEta, &b_clusterEta);
   fChain->SetBranchAddress("clusterPhi", &clusterPhi, &b_clusterPhi);
   fChain->SetBranchAddress("cluster_sumCellE", &cluster_sumCellE, &b_cluster_sumCellE);
   fChain->SetBranchAddress("cluster_EM_PROBABILITY", &cluster_EM_PROBABILITY, &b_cluster_EM_PROBABILITY);
   fChain->SetBranchAddress("Blacluster_HAD_WEIGHTnk", &Blacluster_HAD_WEIGHTnk, &b_cluster_HAD_WEIGHT);
   fChain->SetBranchAddress("cluster_OOC_WEIGHT", &cluster_OOC_WEIGHT, &b_cluster_OOC_WEIGHT);
   fChain->SetBranchAddress("cluster_DM_WEIGHT", &cluster_DM_WEIGHT, &b_cluster_DM_WEIGHT);
   fChain->SetBranchAddress("cluster_ENG_CALIB_TOT", &cluster_ENG_CALIB_TOT, &b_cluster_ENG_CALIB_TOT);
   fChain->SetBranchAddress("cluster_ENG_CALIB_OUT_T", &cluster_ENG_CALIB_OUT_T, &b_cluster_ENG_CALIB_OUT_T);
   fChain->SetBranchAddress("cluster_ENG_CALIB_DEAD_TOT", &cluster_ENG_CALIB_DEAD_TOT, &b_cluster_ENG_CALIB_DEAD_TOT);
   fChain->SetBranchAddress("cluster_CENTER_MAG", &cluster_CENTER_MAG, &b_cluster_CENTER_MAG);
   fChain->SetBranchAddress("cluster_FIRST_ENG_DENS", &cluster_FIRST_ENG_DENS, &b_cluster_FIRST_ENG_DENS);
   fChain->SetBranchAddress("cluster_FIRST_PHI", &cluster_FIRST_PHI, &b_cluster_FIRST_PHI);
   fChain->SetBranchAddress("cluster_FIRST_ETA", &cluster_FIRST_ETA, &b_cluster_FIRST_ETA);
   fChain->SetBranchAddress("cluster_SECOND_R", &cluster_SECOND_R, &b_cluster_SECOND_R);
   fChain->SetBranchAddress("cluster_SECOND_LAMBDA", &cluster_SECOND_LAMBDA, &b_cluster_SECOND_LAMBDA);
   fChain->SetBranchAddress("cluster_DELTA_PHI", &cluster_DELTA_PHI, &b_cluster_DELTA_PHI);
   fChain->SetBranchAddress("cluster_DELTA_THETA", &cluster_DELTA_THETA, &b_cluster_DELTA_THETA);
   fChain->SetBranchAddress("cluster_DELTA_ALPHA", &cluster_DELTA_ALPHA, &b_cluster_DELTA_ALPHA);
   fChain->SetBranchAddress("cluster_CENTER_X", &cluster_CENTER_X, &b_cluster_CENTER_X);
   fChain->SetBranchAddress("cluster_CENTER_Y", &cluster_CENTER_Y, &b_cluster_CENTER_Y);
   fChain->SetBranchAddress("cluster_CENTER_Z", &cluster_CENTER_Z, &b_cluster_CENTER_Z);
   fChain->SetBranchAddress("cluster_CENTER_LAMBDA", &cluster_CENTER_LAMBDA, &b_cluster_CENTER_LAMBDA);
   fChain->SetBranchAddress("cluster_LATERAL", &cluster_LATERAL, &b_cluster_LATERAL);
   fChain->SetBranchAddress("cluster_LONGITUDINAL", &cluster_LONGITUDINAL, &b_cluster_LONGITUDINAL);
   fChain->SetBranchAddress("cluster_ENG_FRAC_EM", &cluster_ENG_FRAC_EM, &b_cluster_ENG_FRAC_EM);
   fChain->SetBranchAddress("cluster_ENG_FRAC_MAX", &cluster_ENG_FRAC_MAX, &b_cluster_ENG_FRAC_MAX);
   fChain->SetBranchAddress("cluster_ENG_FRAC_CORE", &cluster_ENG_FRAC_CORE, &b_cluster_ENG_FRAC_CORE);
   fChain->SetBranchAddress("cluster_SECOND_ENG_DENS", &cluster_SECOND_ENG_DENS, &b_cluster_SECOND_ENG_DENS);
   fChain->SetBranchAddress("cluster_ISOLATION", &cluster_ISOLATION, &b_cluster_ISOLATION);
   fChain->SetBranchAddress("cluster_ENG_BAD_CELLS", &cluster_ENG_BAD_CELLS, &b_cluster_ENG_BAD_CELLS);
   fChain->SetBranchAddress("cluster_N_BAD_CELLS", &cluster_N_BAD_CELLS, &b_cluster_N_BAD_CELLS);
   fChain->SetBranchAddress("cluster_N_BAD_CELLS_CORR", &cluster_N_BAD_CELLS_CORR, &b_cluster_N_BAD_CELLS_CORR);
   fChain->SetBranchAddress("cluster_BAD_CELLS_CORR_E", &cluster_BAD_CELLS_CORR_E, &b_cluster_BAD_CELLS_CORR_E);
   fChain->SetBranchAddress("cluster_BADLARQ_FRAC", &cluster_BADLARQ_FRAC, &b_cluster_BADLARQ_FRAC);
   fChain->SetBranchAddress("cluster_ENG_POS", &cluster_ENG_POS, &b_cluster_ENG_POS);
   fChain->SetBranchAddress("cluster_SIGNIFICANCE", &cluster_SIGNIFICANCE, &b_cluster_SIGNIFICANCE);
   fChain->SetBranchAddress("cluster_CELL_SIGNIFICANCE", &cluster_CELL_SIGNIFICANCE, &b_cluster_CELL_SIGNIFICANCE);
   fChain->SetBranchAddress("cluster_CELL_SIG_SAMPLING", &cluster_CELL_SIG_SAMPLING, &b_cluster_CELL_SIG_SAMPLING);
   fChain->SetBranchAddress("cluster_AVG_LAR_Q", &cluster_AVG_LAR_Q, &b_cluster_AVG_LAR_Q);
   fChain->SetBranchAddress("cluster_AVG_TILE_Q", &cluster_AVG_TILE_Q, &b_cluster_AVG_TILE_Q);
   fChain->SetBranchAddress("cluster_ENG_BAD_HV_CELLS", &cluster_ENG_BAD_HV_CELLS, &b_cluster_ENG_BAD_HV_CELLS);
   fChain->SetBranchAddress("cluster_N_BAD_HV_CELLS", &cluster_N_BAD_HV_CELLS, &b_cluster_N_BAD_HV_CELLS);
   fChain->SetBranchAddress("cluster_PTD", &cluster_PTD, &b_cluster_PTD);
   fChain->SetBranchAddress("cluster_MASS", &cluster_MASS, &b_cluster_MASS);
   fChain->SetBranchAddress("EM_Shower", &EM_Shower, &b_EM_Shower);
   fChain->SetBranchAddress("EM_Pro", &EM_Pro, &b_EM_Pro);
   fChain->SetBranchAddress("CalibratedE", &CalibratedE, &b_CalibratedE);
   fChain->SetBranchAddress("Delta_E", &Delta_E, &b_Delta_E);
   fChain->SetBranchAddress("Delta_Calib_E", &Delta_Calib_E, &b_Delta_Calib_E);
   Notify();
}

Bool_t ClusterPlotsHAD::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ClusterPlotsHAD::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ClusterPlotsHAD::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ClusterPlotsHAD_cxx
