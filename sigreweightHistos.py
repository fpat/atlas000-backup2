import os
import sys
import glob
import ROOT

orig_sample_path = "/home/felixpat10/Samples_JSS/"
histo_sample_path = "/home/felixpat10/Samples_JSS"

#Loop over each of the dijet slices (12 in total)
for i in range (0,8):
      
    ##############################################################
    #First let's get the number of weighted events for each sample
    ##############################################################
  
    rootfiles=glob.glob('/home/felixpat10/Samples_JSS/Zprime_B*')

    eventCount = 0.0

    #Loop over all the ROOT files in one dataset
    for rootfile in rootfiles:
        #Open the ROOT file
        f =ROOT.TFile.Open(rootfile)
	print rootfile 
       #Get the histogram from the original file that contains the number of weighted events
        MetaData_EventCount=f.Get('MetaData_EventCount_Test')
        eventCount += MetaData_EventCount.GetBinContent(3)
        f.Close()

    print eventCount

    ##############################################################
    #Second step: reweight all the histograms that you have crated
    ##############################################################

    histoName = 'h2_sig'+str(i)+'.root' #You have to replace this with the name of your output file, it should include some sort of naming that reflects the dijet slice
    histoName_weighted = 'h2sigddt_weighted.root'

    #File that will contain the original histograms but reweighted
    fh_out =ROOT.TFile.Open(histo_sample_path+'/'+histoName_weighted,'recreate')
    #Original ROOT file
    fh_hist=ROOT.TFile.Open(histo_sample_path+'/'+histoName)

    #List all the objects within the ROOT file
    keys=fh_hist.GetListOfKeys()

    for key in keys:
        name=key.GetName()
        obj=key.ReadObj()
        if obj.InheritsFrom(ROOT.TH1.Class()) or obj.InheritsFrom(ROOT.TH2.Class()) or obj.InheritsFrom(ROOT.TProfile.Class()):
            fh_out.cd()
            if(eventCount!=0): obj.Scale(1./eventCount)
            obj.Write() #write to output file
        #obj.Delete()	
	
    fh_out.Close()
