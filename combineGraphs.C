
// This program is to plot original signal and background plots on one canvas


   
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <iostream>
//#include <TH2.h>
//#include <TStyle.h>
//#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TFile.h>
#include <Rtypes.h>
//#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <TreeReader.h>
#include <math.h>

void combineGraphs(){

	/*TFile f1("Event.root");
	h_jet_massS->Draw("same");

	//gpad->SetLogy();	
	double norm = 1;
	double scale = norm / (h_jet_massS->Integral());
	h_jet_massS->Scale(scale);

	f1.Close();

	TFile f2("outputFile.root");
	h_jet_massB->Draw("same");

	//gpad->SetLogy();
	scale = norm / (h_jet_massB->Integral());
	h_jet_massB->Scale(scale);

	f2.Close();*/	

	cout << "Starting to combine" << endl;
	TH1D* h_sig = new TH1D;//("Signal Efficiency ; Background Efficiency");
	TH1D* h_bg = new TH1D; //edit: signal count total ; background count total
	TH1D* h_sig2 = new TH1D;
	TH1D* h_bg2= new TH1D;
	
	TH1D* g1 = new TH1D;
	TH1D* g2 = new TH1D;
	TH1F* g3 = new TH1F;
	TH1F* g4 = new TH1F;
	TH1F* g5 = new TH1F;

	TFile f1("outputFile.root");
	g1 = h_jet_massB;
	g1->SetDirectory(0);
	f1.Close();

	TFile f2("2DoutputFile_Tau21_Cut.root");
	g2 = h_jet_massS_Cut_Tau21;
	g2->SetDirectory(0);
	f2.Close();
	/*	
	TFile f3("Sig_Tau21_wta.root");
	g3 = Sig_Tau21_wta;
	//g3->SetDirectory(0);
	f3.Close();
	
	TFile f4("Sig_Mu12.root");
	g4 = Sig_Mu12;
	//g4->SetDirectory(0);
	f4.Close();
	
	TFile f5("Sig_Split12.root");
	g5 = Sig_Split12;
	//g5->SetDirectory(0);
	f5.Close();
*/
/*
	
	TFile f1("Event.root");
	h_sig = h_jet_massS;
	h_sig->SetDirectory(0);
	f1.Close();
	
	TFile f2("outputFile.root");
	h_bg = h_jet_massB;
	h_bg->SetDirectory(0);
	f2.Close();
	
	TFile f3("Event_Split12.root");
//f1.cd("h_jet_massS");

	//h_sig = h_jet_D2S_Cut;
	h_sig2 = h_jet_massS_Cut_Split12;
	h_sig2->SetDirectory(0);

	f3.Close();
	
	TFile f4("outputFile_Tau21_wta.root");
//f2.cd("h_jet_mass_pt4");

	//h_bg = h_jet_D2B_Cut;
	h_bg2 = h_jet_massB_Cut_Tau21_wta;
	h_bg2->SetDirectory(0);
   
	f4.Close();
	*/
//	const int n = 100;
	//h_sig->Scale(1. / h_sig->Integral());
	//h_bg->Scale(1. / h_bg->Integral());
	
	//h_sig2->Scale(1. / h_sig2->Integral());
	//h_bg2->Scale(1. / h_bg2->Integral());
	//h_sig->SetLineColor(2);
	//h_sig->Draw("same");
	//h_bg->Draw("same");
//	double allS = h_sig->Integral(0, n + 1);
   	
	TFile *f_out = new TFile("2DCombine_Tau21.root", "RECREATE");
   
	f_out->cd();
	g1->SetLineColor(1);
	//h_bg->SetLineColor(2);
	g2->SetLineColor(2);
	//h_bg2->SetLineColor(4);
	//h_sig->Write();
	
	//h_sig->SetTitle("Signal_Split12 Jet Mass; jet mass [GeV]; number of jets");	
	//h_sig2->SetTitle("Signal_Split12 Cut at 45*10^3 [MeV]; jet mass [GeV]; number of jets");
	h_bg->SetTitle("Roc Curves; Signal Efficiency; 1 - Background Efficiency");	
	h_bg2->SetTitle("Signal_Tau21_wta Cut at 0.2; jet mass [GeV]; number of jets");
	g1->SetTitle("Background Jet Mass");
	g2->SetTitle("Background Jet Mass after #tau_{21}^{ddt} cut at -0.181");
	//h_sig2->Draw("same");
	//h_sig->Draw("same");
	//h_bg->Draw("same");
	//h_sig2->Draw("same");
	//h_bg2->Draw("same");	
	
	g1->Draw("same");
	g2->Draw("same");
	//g3->Draw("same");
	//g4->Draw("same");
	//g5->Draw("same");
	c1->BuildLegend();
/*	auto legend = new TLegend(.1,.7,.4,.9);
	legend->SetHeader("Normalized sigVSbg C2");
	legend->AddEntry(h_sig, "sig = blue line", "l");
	legend->AddEntry(h_bg, "bg = red line","l");
	legend->Draw("same");
*/
	
	//h_bg->Draw("same");
	//c1->SetName("Signal Mass Cut Tau21_wta");
	//gStyle->SetTitle("Signal Mass Cut Tau21_wta");
	TPaveText *title = new TPaveText(0.0,.9,.3,1.0,"brNDC");
//	title->AddText("Signal Jet Mass before and after Cut Tau21_wta at 0.2");
	//title->Draw("same");
	c1->Write();	
	f_out->Write();
	f_out->Close();
	
	cout <<"Finished"<< endl;
//	double allB = h_bg->Integral(0, n + 1);

}
