//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Nov  5 08:41:54 2020 by ROOT version 5.34/30
// from TTree outTree/outTree
// found on file: Zprime_B10/user.cdelitzs.22900808._000001.tree.root
//////////////////////////////////////////////////////////

#ifndef TreeReader_ZprimeB_h
#define TreeReader_ZprimeB_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class TreeReader_ZprimeB {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           runNumber;
   Long64_t        eventNumber;
   Int_t           lumiBlock;
   UInt_t          coreFlags;
   Int_t           bcid;
   Int_t           mcEventNumber;
   Int_t           mcChannelNumber;
   Float_t         mcEventWeight;
   vector<float>   *mcEventWeights;
   Int_t           NPV;
   Float_t         actualInteractionsPerCrossing;
   Float_t         averageInteractionsPerCrossing;
   Float_t         weight_pileup;
   Float_t         correctedAverageMu;
   Float_t         correctedAndScaledAverageMu;
   Float_t         correctedActualMu;
   Float_t         correctedAndScaledActualMu;
   Int_t           rand_run_nr;
   Int_t           rand_lumiblock_nr;
   Float_t         weight;
   Float_t         weight_xs;
   Float_t         Zprime_pt;
   Float_t         Zprime_eta;
   Float_t         Zprime_phi;
   Float_t         Zprime_m;
   UInt_t          Zprime_pdg;
   Float_t         reso0_pt;
   Float_t         reso0_eta;
   Float_t         reso0_phi;
   Float_t         reso0_E;
   Float_t         reso1_pt;
   Float_t         reso1_eta;
   Float_t         reso1_phi;
   Float_t         reso1_E;
   Float_t         ISR_pt;
   Float_t         ISR_eta;
   Float_t         ISR_phi;
   Float_t         ISR_E;
   Int_t           ISR_pdgId;
   vector<string>  *passedTriggers;
   vector<string>  *disabledTriggers;
   vector<unsigned int> *isPassBits;
   vector<string>  *isPassBitsNames;
   Int_t           njet;
   vector<float>   *jet_E;
   vector<float>   *jet_pt;
   vector<float>   *jet_phi;
   vector<float>   *jet_eta;
   vector<float>   *jet_Timing;
   vector<float>   *jet_LArQuality;
   vector<float>   *jet_HECQuality;
   vector<float>   *jet_NegativeE;
   vector<float>   *jet_AverageLArQF;
   vector<float>   *jet_BchCorrCell;
   vector<float>   *jet_N90Constituents;
   vector<float>   *jet_LArBadHVEnergyFrac;
   vector<int>     *jet_LArBadHVNCell;
   vector<float>   *jet_OotFracClusters5;
   vector<float>   *jet_OotFracClusters10;
   vector<float>   *jet_LeadingClusterPt;
   vector<float>   *jet_LeadingClusterSecondLambda;
   vector<float>   *jet_LeadingClusterCenterLambda;
   vector<float>   *jet_LeadingClusterSecondR;
   vector<int>     *jet_clean_passLooseBadUgly;
   vector<int>     *jet_clean_passTightBadUgly;
   vector<int>     *jet_clean_passLooseBad;
   vector<int>     *jet_clean_passTightBad;
   vector<int>     *jet_ConeTruthLabelID;
   vector<int>     *jet_TruthCount;
   vector<float>   *jet_TruthLabelDeltaR_B;
   vector<float>   *jet_TruthLabelDeltaR_C;
   vector<float>   *jet_TruthLabelDeltaR_T;
   vector<int>     *jet_PartonTruthLabelID;
   vector<float>   *jet_GhostTruthAssociationFraction;
   vector<float>   *jet_truth_E;
   vector<float>   *jet_truth_pt;
   vector<float>   *jet_truth_phi;
   vector<float>   *jet_truth_eta;
   Int_t           nfatjet;
   vector<float>   *fatjet_m;
   vector<float>   *fatjet_pt;
   vector<float>   *fatjet_phi;
   vector<float>   *fatjet_eta;
   vector<float>   *fatjet_Split12;
   vector<float>   *fatjet_Split23;
   vector<float>   *fatjet_Split34;
   vector<float>   *fatjet_tau1_wta;
   vector<float>   *fatjet_tau2_wta;
   vector<float>   *fatjet_tau3_wta;
   vector<float>   *fatjet_tau21_wta;
   vector<float>   *fatjet_tau32_wta;
   vector<float>   *fatjet_ECF1;
   vector<float>   *fatjet_ECF2;
   vector<float>   *fatjet_ECF3;
   vector<float>   *fatjet_C2;
   vector<float>   *fatjet_D2;
   vector<float>   *fatjet_NTrimSubjets;
   vector<int>     *fatjet_Nclusters;
   vector<int>     *fatjet_nTracks;
   vector<int>     *fatjet_ungrtrk500;
   vector<float>   *fatjet_EMFrac;
   vector<int>     *fatjet_nChargedParticles;
   vector<int>     *fatjet_numConstituents;
   vector<vector<float> > *fatjet_constituentWeights;
   vector<vector<float> > *fatjet_constituent_pt;
   vector<vector<float> > *fatjet_constituent_eta;
   vector<vector<float> > *fatjet_constituent_phi;
   vector<vector<float> > *fatjet_constituent_e;
   vector<int>     *fatjet_nTQuarks;
   vector<int>     *fatjet_nHBosons;
   vector<int>     *fatjet_nWBosons;
   vector<int>     *fatjet_nZBosons;
   vector<float>   *fatjet_ANN_score;
   Int_t           ntruth_fatjet;
   vector<float>   *truth_fatjet_E;
   vector<float>   *truth_fatjet_pt;
   vector<float>   *truth_fatjet_phi;
   vector<float>   *truth_fatjet_eta;
   vector<float>   *truth_fatjet_m;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_mcEventNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mcEventWeight;   //!
   TBranch        *b_mcEventWeights;   //!
   TBranch        *b_NPV;   //!
   TBranch        *b_actualInteractionsPerCrossing;   //!
   TBranch        *b_averageInteractionsPerCrossing;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_correctedAverageMu;   //!
   TBranch        *b_correctedAndScaledAverageMu;   //!
   TBranch        *b_correctedActualMu;   //!
   TBranch        *b_correctedAndScaledActualMu;   //!
   TBranch        *b_rand_run_nr;   //!
   TBranch        *b_rand_lumiblock_nr;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_weight_xs;   //!
   TBranch        *b_Zprime_pt;   //!
   TBranch        *b_Zprime_eta;   //!
   TBranch        *b_Zprime_phi;   //!
   TBranch        *b_Zprime_m;   //!
   TBranch        *b_Zprime_pdg;   //!
   TBranch        *b_reso0_pt;   //!
   TBranch        *b_reso0_eta;   //!
   TBranch        *b_reso0_phi;   //!
   TBranch        *b_reso0_E;   //!
   TBranch        *b_reso1_pt;   //!
   TBranch        *b_reso1_eta;   //!
   TBranch        *b_reso1_phi;   //!
   TBranch        *b_reso1_E;   //!
   TBranch        *b_ISR_pt;   //!
   TBranch        *b_ISR_eta;   //!
   TBranch        *b_ISR_phi;   //!
   TBranch        *b_ISR_E;   //!
   TBranch        *b_ISR_pdgId;   //!
   TBranch        *b_passedTriggers;   //!
   TBranch        *b_disabledTriggers;   //!
   TBranch        *b_isPassBits;   //!
   TBranch        *b_isPassBitsNames;   //!
   TBranch        *b_njet;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_Timing;   //!
   TBranch        *b_jet_LArQuality;   //!
   TBranch        *b_jet_HECQuality;   //!
   TBranch        *b_jet_NegativeE;   //!
   TBranch        *b_jet_AverageLArQF;   //!
   TBranch        *b_jet_BchCorrCell;   //!
   TBranch        *b_jet_N90Constituents;   //!
   TBranch        *b_jet_LArBadHVEnergyFrac;   //!
   TBranch        *b_jet_LArBadHVNCell;   //!
   TBranch        *b_jet_OotFracClusters5;   //!
   TBranch        *b_jet_OotFracClusters10;   //!
   TBranch        *b_jet_LeadingClusterPt;   //!
   TBranch        *b_jet_LeadingClusterSecondLambda;   //!
   TBranch        *b_jet_LeadingClusterCenterLambda;   //!
   TBranch        *b_jet_LeadingClusterSecondR;   //!
   TBranch        *b_jet_clean_passLooseBadUgly;   //!
   TBranch        *b_jet_clean_passTightBadUgly;   //!
   TBranch        *b_jet_clean_passLooseBad;   //!
   TBranch        *b_jet_clean_passTightBad;   //!
   TBranch        *b_jet_ConeTruthLabelID;   //!
   TBranch        *b_jet_TruthCount;   //!
   TBranch        *b_jet_TruthLabelDeltaR_B;   //!
   TBranch        *b_jet_TruthLabelDeltaR_C;   //!
   TBranch        *b_jet_TruthLabelDeltaR_T;   //!
   TBranch        *b_jet_PartonTruthLabelID;   //!
   TBranch        *b_jet_GhostTruthAssociationFraction;   //!
   TBranch        *b_jet_truth_E;   //!
   TBranch        *b_jet_truth_pt;   //!
   TBranch        *b_jet_truth_phi;   //!
   TBranch        *b_jet_truth_eta;   //!
   TBranch        *b_nfatjet;   //!
   TBranch        *b_fatjet_m;   //!
   TBranch        *b_fatjet_pt;   //!
   TBranch        *b_fatjet_phi;   //!
   TBranch        *b_fatjet_eta;   //!
   TBranch        *b_fatjet_Split12;   //!
   TBranch        *b_fatjet_Split23;   //!
   TBranch        *b_fatjet_Split34;   //!
   TBranch        *b_fatjet_tau1_wta;   //!
   TBranch        *b_fatjet_tau2_wta;   //!
   TBranch        *b_fatjet_tau3_wta;   //!
   TBranch        *b_fatjet_tau21_wta;   //!
   TBranch        *b_fatjet_tau32_wta;   //!
   TBranch        *b_fatjet_ECF1;   //!
   TBranch        *b_fatjet_ECF2;   //!
   TBranch        *b_fatjet_ECF3;   //!
   TBranch        *b_fatjet_C2;   //!
   TBranch        *b_fatjet_D2;   //!
   TBranch        *b_fatjet_NTrimSubjets;   //!
   TBranch        *b_fatjet_Nclusters;   //!
   TBranch        *b_fatjet_nTracks;   //!
   TBranch        *b_fatjet_ungrtrk500;   //!
   TBranch        *b_fatjet_EMFrac;   //!
   TBranch        *b_fatjet_nChargedParticles;   //!
   TBranch        *b_fatjet_numConstituents;   //!
   TBranch        *b_fatjet_constituentWeights;   //!
   TBranch        *b_fatjet_constituent_pt;   //!
   TBranch        *b_fatjet_constituent_eta;   //!
   TBranch        *b_fatjet_constituent_phi;   //!
   TBranch        *b_fatjet_constituent_e;   //!
   TBranch        *b_fatjet_nTQuarks;   //!
   TBranch        *b_fatjet_nHBosons;   //!
   TBranch        *b_fatjet_nWBosons;   //!
   TBranch        *b_fatjet_nZBosons;   //!
   TBranch        *b_fatjet_ANN_score;   //!
   TBranch        *b_ntruth_fatjet;   //!
   TBranch        *b_truth_fatjet_E;   //!
   TBranch        *b_truth_fatjet_pt;   //!
   TBranch        *b_truth_fatjet_phi;   //!
   TBranch        *b_truth_fatjet_eta;   //!
   TBranch        *b_truth_fatjet_m;   //!

   TreeReader_ZprimeB(TTree *tree=0);
   virtual ~TreeReader_ZprimeB();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TreeReader_ZprimeB_cxx
TreeReader_ZprimeB::TreeReader_ZprimeB(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Zprime_B10/user.cdelitzs.22900808._000001.tree.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Zprime_B10/user.cdelitzs.22900808._000001.tree.root");
      }
      f->GetObject("outTree",tree);

   }
   Init(tree);
}

TreeReader_ZprimeB::~TreeReader_ZprimeB()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TreeReader_ZprimeB::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TreeReader_ZprimeB::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TreeReader_ZprimeB::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mcEventWeights = 0;
   passedTriggers = 0;
   disabledTriggers = 0;
   isPassBits = 0;
   isPassBitsNames = 0;
   jet_E = 0;
   jet_pt = 0;
   jet_phi = 0;
   jet_eta = 0;
   jet_Timing = 0;
   jet_LArQuality = 0;
   jet_HECQuality = 0;
   jet_NegativeE = 0;
   jet_AverageLArQF = 0;
   jet_BchCorrCell = 0;
   jet_N90Constituents = 0;
   jet_LArBadHVEnergyFrac = 0;
   jet_LArBadHVNCell = 0;
   jet_OotFracClusters5 = 0;
   jet_OotFracClusters10 = 0;
   jet_LeadingClusterPt = 0;
   jet_LeadingClusterSecondLambda = 0;
   jet_LeadingClusterCenterLambda = 0;
   jet_LeadingClusterSecondR = 0;
   jet_clean_passLooseBadUgly = 0;
   jet_clean_passTightBadUgly = 0;
   jet_clean_passLooseBad = 0;
   jet_clean_passTightBad = 0;
   jet_ConeTruthLabelID = 0;
   jet_TruthCount = 0;
   jet_TruthLabelDeltaR_B = 0;
   jet_TruthLabelDeltaR_C = 0;
   jet_TruthLabelDeltaR_T = 0;
   jet_PartonTruthLabelID = 0;
   jet_GhostTruthAssociationFraction = 0;
   jet_truth_E = 0;
   jet_truth_pt = 0;
   jet_truth_phi = 0;
   jet_truth_eta = 0;
   fatjet_m = 0;
   fatjet_pt = 0;
   fatjet_phi = 0;
   fatjet_eta = 0;
   fatjet_Split12 = 0;
   fatjet_Split23 = 0;
   fatjet_Split34 = 0;
   fatjet_tau1_wta = 0;
   fatjet_tau2_wta = 0;
   fatjet_tau3_wta = 0;
   fatjet_tau21_wta = 0;
   fatjet_tau32_wta = 0;
   fatjet_ECF1 = 0;
   fatjet_ECF2 = 0;
   fatjet_ECF3 = 0;
   fatjet_C2 = 0;
   fatjet_D2 = 0;
   fatjet_NTrimSubjets = 0;
   fatjet_Nclusters = 0;
   fatjet_nTracks = 0;
   fatjet_ungrtrk500 = 0;
   fatjet_EMFrac = 0;
   fatjet_nChargedParticles = 0;
   fatjet_numConstituents = 0;
   fatjet_constituentWeights = 0;
   fatjet_constituent_pt = 0;
   fatjet_constituent_eta = 0;
   fatjet_constituent_phi = 0;
   fatjet_constituent_e = 0;
   fatjet_nTQuarks = 0;
   fatjet_nHBosons = 0;
   fatjet_nWBosons = 0;
   fatjet_nZBosons = 0;
   fatjet_ANN_score = 0;
   truth_fatjet_E = 0;
   truth_fatjet_pt = 0;
   truth_fatjet_phi = 0;
   truth_fatjet_eta = 0;
   truth_fatjet_m = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("mcEventNumber", &mcEventNumber, &b_mcEventNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mcEventWeight", &mcEventWeight, &b_mcEventWeight);
   fChain->SetBranchAddress("mcEventWeights", &mcEventWeights, &b_mcEventWeights);
   fChain->SetBranchAddress("NPV", &NPV, &b_NPV);
   fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("correctedAverageMu", &correctedAverageMu, &b_correctedAverageMu);
   fChain->SetBranchAddress("correctedAndScaledAverageMu", &correctedAndScaledAverageMu, &b_correctedAndScaledAverageMu);
   fChain->SetBranchAddress("correctedActualMu", &correctedActualMu, &b_correctedActualMu);
   fChain->SetBranchAddress("correctedAndScaledActualMu", &correctedAndScaledActualMu, &b_correctedAndScaledActualMu);
   fChain->SetBranchAddress("rand_run_nr", &rand_run_nr, &b_rand_run_nr);
   fChain->SetBranchAddress("rand_lumiblock_nr", &rand_lumiblock_nr, &b_rand_lumiblock_nr);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("weight_xs", &weight_xs, &b_weight_xs);
   fChain->SetBranchAddress("Zprime_pt", &Zprime_pt, &b_Zprime_pt);
   fChain->SetBranchAddress("Zprime_eta", &Zprime_eta, &b_Zprime_eta);
   fChain->SetBranchAddress("Zprime_phi", &Zprime_phi, &b_Zprime_phi);
   fChain->SetBranchAddress("Zprime_m", &Zprime_m, &b_Zprime_m);
   fChain->SetBranchAddress("Zprime_pdg", &Zprime_pdg, &b_Zprime_pdg);
   fChain->SetBranchAddress("reso0_pt", &reso0_pt, &b_reso0_pt);
   fChain->SetBranchAddress("reso0_eta", &reso0_eta, &b_reso0_eta);
   fChain->SetBranchAddress("reso0_phi", &reso0_phi, &b_reso0_phi);
   fChain->SetBranchAddress("reso0_E", &reso0_E, &b_reso0_E);
   fChain->SetBranchAddress("reso1_pt", &reso1_pt, &b_reso1_pt);
   fChain->SetBranchAddress("reso1_eta", &reso1_eta, &b_reso1_eta);
   fChain->SetBranchAddress("reso1_phi", &reso1_phi, &b_reso1_phi);
   fChain->SetBranchAddress("reso1_E", &reso1_E, &b_reso1_E);
   fChain->SetBranchAddress("ISR_pt", &ISR_pt, &b_ISR_pt);
   fChain->SetBranchAddress("ISR_eta", &ISR_eta, &b_ISR_eta);
   fChain->SetBranchAddress("ISR_phi", &ISR_phi, &b_ISR_phi);
   fChain->SetBranchAddress("ISR_E", &ISR_E, &b_ISR_E);
   fChain->SetBranchAddress("ISR_pdgId", &ISR_pdgId, &b_ISR_pdgId);
   fChain->SetBranchAddress("passedTriggers", &passedTriggers, &b_passedTriggers);
   fChain->SetBranchAddress("disabledTriggers", &disabledTriggers, &b_disabledTriggers);
   fChain->SetBranchAddress("isPassBits", &isPassBits, &b_isPassBits);
   fChain->SetBranchAddress("isPassBitsNames", &isPassBitsNames, &b_isPassBitsNames);
   fChain->SetBranchAddress("njet", &njet, &b_njet);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_Timing", &jet_Timing, &b_jet_Timing);
   fChain->SetBranchAddress("jet_LArQuality", &jet_LArQuality, &b_jet_LArQuality);
   fChain->SetBranchAddress("jet_HECQuality", &jet_HECQuality, &b_jet_HECQuality);
   fChain->SetBranchAddress("jet_NegativeE", &jet_NegativeE, &b_jet_NegativeE);
   fChain->SetBranchAddress("jet_AverageLArQF", &jet_AverageLArQF, &b_jet_AverageLArQF);
   fChain->SetBranchAddress("jet_BchCorrCell", &jet_BchCorrCell, &b_jet_BchCorrCell);
   fChain->SetBranchAddress("jet_N90Constituents", &jet_N90Constituents, &b_jet_N90Constituents);
   fChain->SetBranchAddress("jet_LArBadHVEnergyFrac", &jet_LArBadHVEnergyFrac, &b_jet_LArBadHVEnergyFrac);
   fChain->SetBranchAddress("jet_LArBadHVNCell", &jet_LArBadHVNCell, &b_jet_LArBadHVNCell);
   fChain->SetBranchAddress("jet_OotFracClusters5", &jet_OotFracClusters5, &b_jet_OotFracClusters5);
   fChain->SetBranchAddress("jet_OotFracClusters10", &jet_OotFracClusters10, &b_jet_OotFracClusters10);
   fChain->SetBranchAddress("jet_LeadingClusterPt", &jet_LeadingClusterPt, &b_jet_LeadingClusterPt);
   fChain->SetBranchAddress("jet_LeadingClusterSecondLambda", &jet_LeadingClusterSecondLambda, &b_jet_LeadingClusterSecondLambda);
   fChain->SetBranchAddress("jet_LeadingClusterCenterLambda", &jet_LeadingClusterCenterLambda, &b_jet_LeadingClusterCenterLambda);
   fChain->SetBranchAddress("jet_LeadingClusterSecondR", &jet_LeadingClusterSecondR, &b_jet_LeadingClusterSecondR);
   fChain->SetBranchAddress("jet_clean_passLooseBadUgly", &jet_clean_passLooseBadUgly, &b_jet_clean_passLooseBadUgly);
   fChain->SetBranchAddress("jet_clean_passTightBadUgly", &jet_clean_passTightBadUgly, &b_jet_clean_passTightBadUgly);
   fChain->SetBranchAddress("jet_clean_passLooseBad", &jet_clean_passLooseBad, &b_jet_clean_passLooseBad);
   fChain->SetBranchAddress("jet_clean_passTightBad", &jet_clean_passTightBad, &b_jet_clean_passTightBad);
   fChain->SetBranchAddress("jet_ConeTruthLabelID", &jet_ConeTruthLabelID, &b_jet_ConeTruthLabelID);
   fChain->SetBranchAddress("jet_TruthCount", &jet_TruthCount, &b_jet_TruthCount);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_B", &jet_TruthLabelDeltaR_B, &b_jet_TruthLabelDeltaR_B);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_C", &jet_TruthLabelDeltaR_C, &b_jet_TruthLabelDeltaR_C);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_T", &jet_TruthLabelDeltaR_T, &b_jet_TruthLabelDeltaR_T);
   fChain->SetBranchAddress("jet_PartonTruthLabelID", &jet_PartonTruthLabelID, &b_jet_PartonTruthLabelID);
   fChain->SetBranchAddress("jet_GhostTruthAssociationFraction", &jet_GhostTruthAssociationFraction, &b_jet_GhostTruthAssociationFraction);
   fChain->SetBranchAddress("jet_truth_E", &jet_truth_E, &b_jet_truth_E);
   fChain->SetBranchAddress("jet_truth_pt", &jet_truth_pt, &b_jet_truth_pt);
   fChain->SetBranchAddress("jet_truth_phi", &jet_truth_phi, &b_jet_truth_phi);
   fChain->SetBranchAddress("jet_truth_eta", &jet_truth_eta, &b_jet_truth_eta);
   fChain->SetBranchAddress("nfatjet", &nfatjet, &b_nfatjet);
   fChain->SetBranchAddress("fatjet_m", &fatjet_m, &b_fatjet_m);
   fChain->SetBranchAddress("fatjet_pt", &fatjet_pt, &b_fatjet_pt);
   fChain->SetBranchAddress("fatjet_phi", &fatjet_phi, &b_fatjet_phi);
   fChain->SetBranchAddress("fatjet_eta", &fatjet_eta, &b_fatjet_eta);
   fChain->SetBranchAddress("fatjet_Split12", &fatjet_Split12, &b_fatjet_Split12);
   fChain->SetBranchAddress("fatjet_Split23", &fatjet_Split23, &b_fatjet_Split23);
   fChain->SetBranchAddress("fatjet_Split34", &fatjet_Split34, &b_fatjet_Split34);
   fChain->SetBranchAddress("fatjet_tau1_wta", &fatjet_tau1_wta, &b_fatjet_tau1_wta);
   fChain->SetBranchAddress("fatjet_tau2_wta", &fatjet_tau2_wta, &b_fatjet_tau2_wta);
   fChain->SetBranchAddress("fatjet_tau3_wta", &fatjet_tau3_wta, &b_fatjet_tau3_wta);
   fChain->SetBranchAddress("fatjet_tau21_wta", &fatjet_tau21_wta, &b_fatjet_tau21_wta);
   fChain->SetBranchAddress("fatjet_tau32_wta", &fatjet_tau32_wta, &b_fatjet_tau32_wta);
   fChain->SetBranchAddress("fatjet_ECF1", &fatjet_ECF1, &b_fatjet_ECF1);
   fChain->SetBranchAddress("fatjet_ECF2", &fatjet_ECF2, &b_fatjet_ECF2);
   fChain->SetBranchAddress("fatjet_ECF3", &fatjet_ECF3, &b_fatjet_ECF3);
   fChain->SetBranchAddress("fatjet_C2", &fatjet_C2, &b_fatjet_C2);
   fChain->SetBranchAddress("fatjet_D2", &fatjet_D2, &b_fatjet_D2);
   fChain->SetBranchAddress("fatjet_NTrimSubjets", &fatjet_NTrimSubjets, &b_fatjet_NTrimSubjets);
   fChain->SetBranchAddress("fatjet_Nclusters", &fatjet_Nclusters, &b_fatjet_Nclusters);
   fChain->SetBranchAddress("fatjet_nTracks", &fatjet_nTracks, &b_fatjet_nTracks);
   fChain->SetBranchAddress("fatjet_ungrtrk500", &fatjet_ungrtrk500, &b_fatjet_ungrtrk500);
   fChain->SetBranchAddress("fatjet_EMFrac", &fatjet_EMFrac, &b_fatjet_EMFrac);
   fChain->SetBranchAddress("fatjet_nChargedParticles", &fatjet_nChargedParticles, &b_fatjet_nChargedParticles);
   fChain->SetBranchAddress("fatjet_numConstituents", &fatjet_numConstituents, &b_fatjet_numConstituents);
   fChain->SetBranchAddress("fatjet_constituentWeights", &fatjet_constituentWeights, &b_fatjet_constituentWeights);
   fChain->SetBranchAddress("fatjet_constituent_pt", &fatjet_constituent_pt, &b_fatjet_constituent_pt);
   fChain->SetBranchAddress("fatjet_constituent_eta", &fatjet_constituent_eta, &b_fatjet_constituent_eta);
   fChain->SetBranchAddress("fatjet_constituent_phi", &fatjet_constituent_phi, &b_fatjet_constituent_phi);
   fChain->SetBranchAddress("fatjet_constituent_e", &fatjet_constituent_e, &b_fatjet_constituent_e);
   fChain->SetBranchAddress("fatjet_nTQuarks", &fatjet_nTQuarks, &b_fatjet_nTQuarks);
   fChain->SetBranchAddress("fatjet_nHBosons", &fatjet_nHBosons, &b_fatjet_nHBosons);
   fChain->SetBranchAddress("fatjet_nWBosons", &fatjet_nWBosons, &b_fatjet_nWBosons);
   fChain->SetBranchAddress("fatjet_nZBosons", &fatjet_nZBosons, &b_fatjet_nZBosons);
   fChain->SetBranchAddress("fatjet_ANN_score", &fatjet_ANN_score, &b_fatjet_ANN_score);
   fChain->SetBranchAddress("ntruth_fatjet", &ntruth_fatjet, &b_ntruth_fatjet);
   fChain->SetBranchAddress("truth_fatjet_E", &truth_fatjet_E, &b_truth_fatjet_E);
   fChain->SetBranchAddress("truth_fatjet_pt", &truth_fatjet_pt, &b_truth_fatjet_pt);
   fChain->SetBranchAddress("truth_fatjet_phi", &truth_fatjet_phi, &b_truth_fatjet_phi);
   fChain->SetBranchAddress("truth_fatjet_eta", &truth_fatjet_eta, &b_truth_fatjet_eta);
   fChain->SetBranchAddress("truth_fatjet_m", &truth_fatjet_m, &b_truth_fatjet_m);
   Notify();
}

Bool_t TreeReader_ZprimeB::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TreeReader_ZprimeB::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TreeReader_ZprimeB::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TreeReader_ZprimeB_cxx
